require 'rails_helper'

RSpec.describe AgentsController, type: :controller do
	describe AgentsController do
		it 'returns one item, the specialty of the agent' do
			agent = Agent.new(name: 'Jorge', status: 'Active', specialty: 'Weapons')

			specialty = agent.getSpecialty

			expect(specialty).to eq 'Blades'
		end
	end
end
